package com.foncsor.helloword.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.foncsor.helloword.model.Deck;
import com.foncsor.helloword.repository.DeckRepository;

@Service("deckService")
public class DeckServiceImpl implements DeckService {

    @Autowired
    private DeckRepository deckRepository;

    @Override
    public void createDeck(Deck deck) {
        deck.setModifiedDate(new Date());
        deck.setCreateDate(new Date());

        deckRepository.save(deck);
    }
}
