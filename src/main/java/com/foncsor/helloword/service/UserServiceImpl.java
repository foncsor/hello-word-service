package com.foncsor.helloword.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.foncsor.helloword.model.User;
import com.foncsor.helloword.repository.UserRepository;
import com.foncsor.helloword.util.UserStatus;

@Service("userService")
public class UserServiceImpl implements UserService {
	
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Override
    public void createUser(User user) {
    	user.setCreateDate(new Date());
    	user.setPassword(passwordEncoder.encode(user.getPassword()));
    	user.setStatus(UserStatus.CREATED.getId());

    	userRepository.save(user);
    }

	@Override
	public void saveUser(User user) {
		userRepository.save(user);
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public User findByUsername(String username) {
        List<User> users = userRepository.findByUsername(username);

        if (users.size() == 0) {
            throw new UsernameNotFoundException("No user found with username " + username);
        }

        return users.get(0);
	}
	
	@Override
	public boolean usernameExists(String username) {
		return userRepository.countByUsername(username) != 0;
	}
	
	@Override
	public boolean emailExists(String email) {
		return userRepository.countByEmail(email) != 0;
	}
}
