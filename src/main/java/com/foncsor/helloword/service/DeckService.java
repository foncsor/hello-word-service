package com.foncsor.helloword.service;

import com.foncsor.helloword.model.Deck;

public interface DeckService {
    void createDeck(Deck deck);
}
