package com.foncsor.helloword.service;

import java.util.List;

import com.foncsor.helloword.model.User;

public interface UserService {
    void saveUser(User user);
    void createUser(User user);
    User findByUsername(String username);
    boolean usernameExists(String username);
    boolean emailExists(String email);
    List<User> findAll();
}
