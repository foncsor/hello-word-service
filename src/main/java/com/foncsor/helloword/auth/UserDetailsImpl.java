package com.foncsor.helloword.auth;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UserDetailsImpl implements UserDetails {

    private String username;
    private String password;
    private int status;
    private Collection<SimpleGrantedAuthority> authorities;

    public UserDetailsImpl() {
    }

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
        // TODO
        return true;
	}

	@Override
	public boolean isAccountNonLocked() {
        // TODO
        return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
        // TODO
        return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO
	    return true;
	    //return status == UserStatus.ACTIVE.getId();
	}

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setAuthorities(Collection<SimpleGrantedAuthority> authorities) {
        this.authorities = authorities;
    }
}
