package com.foncsor.helloword.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.foncsor.helloword.model.User;
import com.foncsor.helloword.service.UserService;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserService userService;

    @Autowired
    Converter<User, UserDetails> userToUserDetails;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return userToUserDetails.convert(userService.findByUsername(username));
	}
}
