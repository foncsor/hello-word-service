package com.foncsor.helloword.util;

public enum UserStatus {
    CREATED(0),
    ACTIVE(1),
	INACTIVE(2),
	DELETED(3);

    private int id;

    UserStatus(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
