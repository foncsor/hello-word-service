package com.foncsor.helloword.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foncsor.helloword.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	List<User> findByUsername(String username);
	long countByUsername(String username);
	long countByEmail(String email);
}
