package com.foncsor.helloword.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foncsor.helloword.model.Deck;

public interface DeckRepository extends JpaRepository<Deck, Long> {

}
