package com.foncsor.helloword.converter;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.foncsor.helloword.auth.UserDetailsImpl;
import com.foncsor.helloword.model.User;

@Component
public class UserToUserDetails implements Converter<User, UserDetails> {

	@Override
	public UserDetails convert(User user) {
        UserDetailsImpl userDetails = new UserDetailsImpl();

        if (user != null) {
            userDetails.setUsername(user.getUsername());
            userDetails.setPassword(user.getPassword());
            userDetails.setStatus(user.getStatus());

            Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();

//            user.getRoles().forEach(role -> {
//                authorities.add(new SimpleGrantedAuthority(role.getName().toString()));
//            });

            userDetails.setAuthorities(authorities);
        }

        return userDetails;
	}
}
