package com.foncsor.helloword.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.foncsor.helloword.controller.exception.EmailAlreadyExistsException;
import com.foncsor.helloword.controller.exception.UsernameAlreadyExistsException;
import com.foncsor.helloword.model.User;
import com.foncsor.helloword.service.UserService;

@RestController
@RequestMapping("api/user")
public class UserController {

	private final Logger LOG = LoggerFactory.getLogger(this.getClass());

	@Autowired
	UserService userService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void register(@RequestBody @Valid User user) {

	    // TODO change/remove
		if (LOG.isInfoEnabled()) {
			LOG.info("POST: api/user endpoint called: " + user.toString());
		}

		if (userService.usernameExists(user.getUsername())) {
			throw new UsernameAlreadyExistsException(user.getUsername());
		}

		if (userService.emailExists(user.getEmail())) {
			throw new EmailAlreadyExistsException(user.getEmail());
		}

		userService.createUser(user);
	}
}
