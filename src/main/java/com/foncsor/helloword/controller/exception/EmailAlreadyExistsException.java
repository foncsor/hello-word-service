package com.foncsor.helloword.controller.exception;

public class EmailAlreadyExistsException extends RuntimeException {
	
    private static final long serialVersionUID = 1L;
	
	public EmailAlreadyExistsException(String email) {
		super("Email already exists in the database: " + email);
	}
}
