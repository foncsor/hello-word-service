package com.foncsor.helloword.controller;

import java.security.Principal;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.foncsor.helloword.model.Deck;
import com.foncsor.helloword.model.User;
import com.foncsor.helloword.service.DeckService;
import com.foncsor.helloword.service.UserService;

@RestController
@RequestMapping("api/deck")
public class DeckController {
    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    DeckService deckService;

    @Autowired
    UserService userService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createDeck(@RequestBody @Valid Deck deck, Principal principal) {

        // TODO change/remove
        if (LOG.isInfoEnabled()) {
            LOG.info("POST: api/deck endpoint called: " + deck.toString());
        }

        User user = userService.findByUsername(principal.getName());

        deck.setUser(user);

        deckService.createDeck(deck);
    }
}
